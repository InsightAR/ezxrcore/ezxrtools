# EZXR Tools

EZXR Tools 易现自研算法工具包，用于生成各类算法配置。

## 对应版本

| Unity Version | EZXR Tools |
| ------------- | ----------- |
| 2022.3        | 0.1.0       |

## Release Note

### V0.1.0

1. 实现Tracking2d 算法配置生成配置

## Tracking2d 算法配置生成工具

#### 第1步：通过Unity Package Manager git url的方式导入EZXR Tools

https://gitlab.com/InsightAR/ezxrcore/ezxrtools.git

#### 第2步：将marker图片导入Unity

特别注意要设置Image的Texture type为Sprite 这样才能正确保证图像的长宽比，如下

![1](doc~/1.png)



#### 第3步：打开窗口 ARSDK/Tools/Image Tracking Gen

![2](doc~/2.png)

配置好导出信息，自定义名称是作为算法SDK 检测成功后返回的marker名字，最后导出算法配置。

#### 第4步：SDK Tracking2d模块配置使用

需要将desc文件和json文件配置到CopyStreamingAssets脚本里，否则会出现无法正确读取算法配置文件。
